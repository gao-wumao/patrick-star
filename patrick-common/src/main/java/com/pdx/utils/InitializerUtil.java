package com.pdx.utils;

import org.springframework.stereotype.Component;

/**
 * @author PDX
 * @website https://blog.csdn.net/Gaowumao
 * @Date 2022-04-30 21:36
 * @Description
 */
@Component
public class InitializerUtil {

    private TokenSetting tokenSetting;

    public InitializerUtil(TokenSetting tokenSetting){
        JwtTokenUtil.setTokenSetting(tokenSetting);
    }
}
