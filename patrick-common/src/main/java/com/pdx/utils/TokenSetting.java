package com.pdx.utils;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.time.Duration;

/**
 * @author PDX
 * @website https://blog.csdn.net/Gaowumao
 * @Date 2022-04-30 21:33
 * @Description
 */
@ConfigurationProperties(prefix = "jwt")
@Component
@Data
public class TokenSetting {

    private String secretKey;
    private Duration accessTokenExpireTime;
    private String issuer;
}
