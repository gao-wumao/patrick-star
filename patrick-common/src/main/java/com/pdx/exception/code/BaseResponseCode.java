package com.pdx.exception.code;

/**
 * @author PDX
 * @website https://blog.csdn.net/Gaowumao
 * @Date 2022-04-30 22:52
 * @Description
 */
public enum BaseResponseCode implements ResponseCodeInterface{
    /*
    * code=0: 服务器已成功处理了请求。通常，这表示服务器提供了请求的
    * code=40010:(授权异常)请求要求身份验证。客户端需要跳转到登录页面去重新登录
    * code=40020:(凭证过期)客户端跳转到登录页面重新登录
    * code=4003x: 没有权限禁止访问
    * code=400xx: 系统主动抛出的业务异常
    * code=50001: 系统异常
    * */
    SUCCESS(0,"操作成功"),
    SYSTEM_ERROR(50001,"系统异常，请稍后再试.."),
    METHOD_IDENTITY_ERROR(40002,"数据校验异常"),
    NOT_PERMISSION(40031,"无权限访问此资源"),
    ACCOUNT_ERROR(400003,"该账号不存在"),
    ACCOUNT_LOCKED(40020,"该账号已被锁定，请联系管理员"),
    ACCOUNT_PASSWORD_ERROR(40004,"用户密码不匹配"),
    ACCOUNT_HAS_DELETE_ERROR(40010,"该账号已被删除，请联系管理员"),
    TOKEN_ERROR(40010,"用户未登录，请重新登录"),
    TOKEN_PAST_DUE(40020,"Token失效，请重新登录"),
    TOKEN_NOT_NULL(40010,"Token不能为空"),
    CAPTCHA_ERROR(40005,"验证码错误"),
    KEY_ALREADY_EXPIRED(40005,"验证码已过期"),
    OPERATION_ERROR(40004,"操作异常"),
    ;



    /*
    * 响应码
    * */
    private int code;

    /*
    * 提示
    * */
    private String msg;

    BaseResponseCode(int code,String msg){
        this.code = code;
        this.msg = msg;
    }


    @Override
    public int getCode() {
        return code;
    }

    @Override
    public String getMsg() {
        return msg;
    }
}
