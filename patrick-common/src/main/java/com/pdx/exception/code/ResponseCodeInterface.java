package com.pdx.exception.code;

/**
 * @author PDX
 * @website https://blog.csdn.net/Gaowumao
 * @Date 2022-04-30 22:49
 * @Description
 */
public interface ResponseCodeInterface {
    int getCode();
    String getMsg();
}
