package com.pdx.constant;

/**
 * @author PDX
 * @website https://blog.csdn.net/Gaowumao
 * @Date 2022-04-30 21:45
 * @Description 常量类
 */
public class Constant {

    /**
     * 用户名称 key
     */
    public static final String JWT_USER_NAME="jwt-user-name-key";
    public static final String ROLES_INFOS_KEY = "roles-infos-key";
    public static final String PERMISSIONS_INFOS_KEY = "permissions-infos-key";
    public static final String DELETE_USER_KEY = "delete-user-key_";
    public static final String ACCOUNT_LOCK_KEY = "account-lock-key_";
    public static final String JWT_ACCESS_TOKEN_BLACKLIST = "jwt-access-token-blacklist_";
    public static final String ACCESS_TOKEN = "authorization";
    public static final String JWT_REFRESH_KEY = "jwt_refresh_key_";
}
