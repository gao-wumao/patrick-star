#   :tw-1f334:  派大星（patrick-star）   :tw-1f334: 
>  :tw-1f34d: 个人介绍：博客：派大星<br>
>  :tw-1f34b: 地址：https://blog.csdn.net/Gaowumao?spm=1000.2115.3001.5343

##  :tw-1f352: 搭建缘由：
对以往技术学习的总结，使用最原始的基础架构一步步从0开始搭建<br>
开发步骤：
-  :tw-1f353: 项目搭建
-  :tw-1f345: 数据库设计
-  :tw-1f349: 权限数据设计
-  :tw-1f347: 接口设计
-  :tw-1f348: 数据添加
-  :tw-1f370: 实际编码
-  :tw-1f367: 单元测试
-  :tw-1f364: 接口测试



##  :tw-1f33b: 介绍
基于Shiro-SpringBoot的权限分配基础框架（0——>1搭建平台）<br>
管理员可以实现对平台人员的权限分配、角色分配、以及部门之间分配<br>
功能初步设想：<br>
-  :tw-1f3b4: 用户登录登出
-  :tw-1f3be: （添加|冻结|删除|修改）用户信息
-  :tw-1f3c0: （添加|冻结|删除|修改）角色信息
-  :tw-1f3c8: （添加|冻结|删除|修改）权限信息
-  :tw-1f3c9: （添加|删除|冻结|修改）部门信心
-   :tw-1f3b3: 对日志的管理
-  :tw-1f3af: 暂时设计如上功能，后续会继续添加


##  :tw-1f40b: 技术选型：
-  :tw-1f40c: SpringBoot
-  :tw-1f40d: Mybatis 编写对接数据库的SQL语句
-  :tw-1f416: MySQL 数据库驱动
-  :tw-1f418: Lombok
-  :tw-1f41d: Swagger2 文档
-  :tw-1f41f: Shiro 安全框架实现用户的鉴权
-  :tw-1f422: Redis NoSQL缓存
-  :tw-1f424: pageHelper 分页插件
-  :tw-1f427: FastJson
-  :tw-1f419: Jwt（Token）令牌

