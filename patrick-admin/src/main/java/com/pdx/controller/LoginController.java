package com.pdx.controller;

import com.pdx.constant.Constant;
import com.pdx.entity.SysUser;
import com.pdx.entity.vo.LoginVo;
import com.pdx.entity.vo.RespBody;
import com.pdx.service.UserService;
import com.pdx.utils.DataResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

/**
 * @author PDX
 * @website https://blog.csdn.net/Gaowumao
 * @Date 2022-05-01 00:11
 * @Description
 */
@RestController
@RequestMapping("/api")
@Api(tags = "用户登录模块",description = "用户登录登出接口")
@Slf4j
public class LoginController {

    @Autowired
    private UserService userService;

    @PostMapping("/user/login")
    @ApiOperation(value = "用户登录接口")
    public DataResult login(@RequestBody @Valid LoginVo vo){
        RespBody token = userService.login(vo);
        return DataResult.success(token);
    }

    @GetMapping("/user/logout")
    @ApiOperation(value = "用户登出")
    public DataResult logout(HttpServletRequest request){
        try {
            String accessToken = request.getHeader(Constant.ACCESS_TOKEN);
            userService.logout(accessToken);
        }catch (Exception e){
            log.error("logout:{}",e);
        }
        return DataResult.success();
    }
}
