package com.pdx.controller;

import com.pdx.constant.Constant;
import com.pdx.entity.SysUser;
import com.pdx.entity.vo.QueryVo;
import com.pdx.entity.vo.UserOwnRoleRespVo;
import com.pdx.entity.vo.UserOwnRoleVo;
import com.pdx.entity.vo.UserUpdateVo;
import com.pdx.service.UserService;
import com.pdx.utils.DataResult;
import com.pdx.utils.JwtTokenUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;
import java.util.Map;

/**
 * @author PDX
 * @website https://blog.csdn.net/Gaowumao
 * @Date 2022-05-02 15:11
 * @Description
 */
@RestController
@Api(tags = "用户模块",description = "用户操作的相关接口")
@RequestMapping("/api/user")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("/users/{pageNo}/{pageSize}")
    @ApiOperation(value = "条件分页查看数据")
    @RequiresPermissions("sys:user:list")
    public DataResult users(@RequestBody(required = false) QueryVo vo, @PathVariable("pageNo")int pageNo,@PathVariable("pageSize")int pageSize){
        Map<String,Object> map = userService.page(vo,pageNo,pageSize);
        return DataResult.success(map);
    }

    @PostMapping("/user")
    @ApiOperation(value = "添加用户接口")
    @RequiresPermissions("sys:user:add")
    public DataResult addUser(@RequestBody(required = false) SysUser sysUser){
        userService.addUser(sysUser);
        return DataResult.success();
    }

    @GetMapping("/user/{id}")
    @ApiOperation(value = "根据id查询用户信息")
    //@RequiresPermissions("sys:user:info")
    public DataResult getInfoById(@PathVariable("id")String id){
        SysUser user = userService.getUserInfoById(id);
        return DataResult.success(user);
    }

    @PutMapping("/user")
    @ApiOperation(value = "修改用户信息")
    @RequiresPermissions("sys:user:update")
    public DataResult updateUser(@RequestBody(required = false) SysUser user){
        String accessToken = (String) SecurityUtils.getSubject().getPrincipal();
        String userId = JwtTokenUtil.getUserId(accessToken);
        userService.updateUser(user,userId);
        return DataResult.success();
    }

    @GetMapping("/user/roles/{userId}")
    @ApiOperation(value = "查询用户拥有的角色数据接口")
    @RequiresPermissions("sys:user:role:update")
    public DataResult getUserOwnRole(@PathVariable("userId")String userId){
        UserOwnRoleRespVo vo = userService.getUserOwnRole(userId);
        return DataResult.success(vo);
    }

    @PutMapping("/user/roles")
    @ApiOperation(value = "保存用户所拥有的角色信息")
    @RequiresPermissions("")
    public DataResult saveUserOwnRole(@RequestBody @Valid UserOwnRoleVo vo){
        userService.setUserOwnRole(vo);
        return DataResult.success();
    }

    @PutMapping("/user")
    @ApiOperation(value = "列表修改用户信息接口")
    @RequiresPermissions("")
    public DataResult updateUserInfo(@RequestBody @Valid UserUpdateVo vo, HttpServletRequest request){
        String accessToken = request.getHeader(Constant.ACCESS_TOKEN);
        String userId = JwtTokenUtil.getUserId(accessToken);
        userService.updateUserInfo(vo,userId);
        return DataResult.success();
    }

    @DeleteMapping("/user")
    @ApiOperation(value = "批量删除用户")
    public DataResult deleteUsers(@RequestBody @ApiParam(value = "用户id集合") List<String> list, HttpServletRequest request){
        String accessToken = request.getHeader(Constant.ACCESS_TOKEN);
        String userId = JwtTokenUtil.getUserId(accessToken);
        userService.deleteUsers(list,userId);
        return DataResult.success();
    }



}
