package com.pdx.entity.vo;

import lombok.Data;

/**
 * @author PDX
 * @website https://blog.csdn.net/Gaowumao
 * @Date 2022-05-01 11:23
 * @Description
 */
@Data
public class RespBody {

    private String id;

    private String username;

    private String accessToken;
}
