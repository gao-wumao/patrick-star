package com.pdx.entity.vo;

import com.pdx.entity.SysRole;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author PDX
 * @website https://blog.csdn.net/Gaowumao
 * @Date 2022-05-03 17:22
 * @Description
 */
@Data
public class UserOwnRoleRespVo {

    @ApiModelProperty(value = "拥有的角色集合")
    private List<String> ownRoles;
    @ApiModelProperty(value = "所拥有的角色列表")
    private List<SysRole> allRole;
}
