package com.pdx.entity.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * @author PDX
 * @website https://blog.csdn.net/Gaowumao
 * @Date 2022-05-09 22:58
 * @Description
 */
@Data
public class UserOwnRoleVo {
    @ApiModelProperty(value = "用户Id")
    @NotBlank(message = "用户ID不能为空")
    private String userId;

    @ApiModelProperty(value = "用户所拥有的角色id")
    private List<String> roleIds;


}
