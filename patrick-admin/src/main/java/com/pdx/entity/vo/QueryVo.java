package com.pdx.entity.vo;

import com.pdx.entity.SysUser;
import lombok.Data;

/**
 * @author PDX
 * @website https://blog.csdn.net/Gaowumao
 * @Date 2022-05-02 15:29
 * @Description
 */
@Data
public class QueryVo extends SysUser {

}
