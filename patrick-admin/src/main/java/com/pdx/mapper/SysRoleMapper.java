package com.pdx.mapper;

import com.pdx.entity.SysRole;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SysRoleMapper {
    int deleteByPrimaryKey(String id);

    int insert(SysRole record);

    int insertSelective(SysRole record);

    SysRole selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(SysRole record);

    int updateByPrimaryKey(SysRole record);

    //根据角色ID集合查询出对应的角色名称
    List<String> selectNamesByIds(List<String> roleIds);

    //查询所有的角色集合
    @Select("select * from sys_role)")
    List<SysRole> selectAll();

}