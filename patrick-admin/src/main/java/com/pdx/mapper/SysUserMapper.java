package com.pdx.mapper;

import com.pdx.entity.SysUser;
import com.pdx.entity.vo.QueryVo;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SysUserMapper {
    int deleteByPrimaryKey(String id);

    int insert(SysUser record);

    int insertSelective(SysUser record);

    SysUser selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(SysUser record);

    int updateByPrimaryKey(SysUser record);

    @Select("select * from sys_user where username = #{username}")
    SysUser getUserByName(@Param("username") String username);

    //查询全部用户
    List<SysUser> selectAll(QueryVo vo);

    @Select("select * from sys_user where id=#{id}")
    SysUser selectUserInfoById(String id);

    //批量删除用户
    int deletedUsers(@Param("user") SysUser user, @Param("list") List<String> list);
}