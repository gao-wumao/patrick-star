package com.pdx.mapper;

import com.pdx.entity.SysRolePermission;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SysRolePermissionMapper {
    int deleteByPrimaryKey(String id);

    int insert(SysRolePermission record);

    int insertSelective(SysRolePermission record);

    SysRolePermission selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(SysRolePermission record);

    int updateByPrimaryKey(SysRolePermission record);
    //根据角色id获取权限资源id列表
    List<String> getPermissionIdsByRoleIds(List<String> roleIdsByUserId);
}