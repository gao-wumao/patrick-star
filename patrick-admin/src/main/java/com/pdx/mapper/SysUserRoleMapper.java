package com.pdx.mapper;

import com.pdx.entity.SysUserRole;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SysUserRoleMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(SysUserRole record);

    int insertSelective(SysUserRole record);

    SysUserRole selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(SysUserRole record);

    int updateByPrimaryKey(SysUserRole record);
    /**
     * 根据用户ID查询出与其对应的所有角色id的列表
     * @param id
     * @return
     */
    List<String> getRoleIdsByUserId(@Param("id") String id);

    /**
     * 根据用户ID删除对应的角色
     * @param userId
     */
    void removeRoleByUserId(String userId);

    /**
     * 批量插入用户角色关联数据
     * @param list
     * @return
     */
    int batchInsertUserRole(List<SysUserRole> list);
}