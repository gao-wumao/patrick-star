package com.pdx.service;

import com.pdx.entity.SysPermission;

import java.util.List;

/**
 * @author PDX
 * @website https://blog.csdn.net/Gaowumao
 * @Date 2022-05-02 13:27
 * @Description
 */
public interface PermissionService {
    /**
     * 根据用户id获取用户权限
     */
    List<String> getPermissionByUserId(String userId);

    List<SysPermission> getPermissions(String userId);
}
