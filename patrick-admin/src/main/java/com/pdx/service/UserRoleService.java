package com.pdx.service;

import com.pdx.entity.vo.UserOwnRoleVo;

import java.util.List;

/**
 * @author PDX
 * @website https://blog.csdn.net/Gaowumao
 * @Date 2022-05-02 13:23
 * @Description
 */
public interface UserRoleService {
    /**
     * 根据用户Id查询出与其对应的所有角色id列表
     * @param id
     * @return
     */
    List<String> getRoleIdsByUserId(String id);


    void addUserRoleInfo(UserOwnRoleVo vo);
}
