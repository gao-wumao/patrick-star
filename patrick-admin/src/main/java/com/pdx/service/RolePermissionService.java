package com.pdx.service;

import java.util.List;

/**
 * @author PDX
 * @website https://blog.csdn.net/Gaowumao
 * @Date 2022-05-02 13:27
 * @Description
 */
public interface RolePermissionService {
    //根据角色id获取权限资源id列表
    List<String> getPermissionIdsByRoleIds(List<String> roleIdsByUserId);

}
