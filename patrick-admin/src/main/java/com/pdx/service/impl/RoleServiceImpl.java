package com.pdx.service.impl;

import com.pdx.entity.SysRole;
import com.pdx.mapper.SysRoleMapper;
import com.pdx.service.RoleService;
import com.pdx.service.UserRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author PDX
 * @website https://blog.csdn.net/Gaowumao
 * @Date 2022-05-02 13:22
 * @Description
 */
@Service
public class RoleServiceImpl implements RoleService {

    @Autowired
    private SysRoleMapper roleMapper;
    @Autowired
    private UserRoleService userRoleService;

    @Override
    public List<String> getNamesByUserId(String id) {
        //根据用户ID通过中间表查询出所有的角色id
        List<String> roleIds =  userRoleService.getRoleIdsByUserId(id);
        if (roleIds.isEmpty()){
            return null;
        }
        return roleMapper.selectNamesByIds(roleIds);
    }

    /**
     * 获取所有的角色集合
     * @return
     */
    @Override
    public List<SysRole> selectAll() {
        List<SysRole> list = roleMapper.selectAll();
        return list;
    }
}
