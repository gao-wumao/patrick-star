package com.pdx.service.impl;

import com.pdx.mapper.SysRolePermissionMapper;
import com.pdx.service.RolePermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author PDX
 * @website https://blog.csdn.net/Gaowumao
 * @Date 2022-05-02 13:27
 * @Description
 */
@Service
public class RolePermissionServiceImpl implements RolePermissionService {
    @Autowired
    private SysRolePermissionMapper rolePermissionMapper;
    /**
     * 根据角色id获取权限资源id列表
     * @param roleIdsByUserId
     * @return
     */
    @Override
    public List<String> getPermissionIdsByRoleIds(List<String> roleIdsByUserId) {
        List<String> list = rolePermissionMapper.getPermissionIdsByRoleIds(roleIdsByUserId);
        return list;
    }
}
