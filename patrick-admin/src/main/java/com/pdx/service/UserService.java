package com.pdx.service;

import com.pdx.entity.SysUser;
import com.pdx.entity.vo.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * @author PDX
 * @website https://blog.csdn.net/Gaowumao
 * @Date 2022-05-01 00:14
 * @Description
 */
public interface UserService {
    /**
     * 登录功能
     * @param vo
     * @return
     */
    RespBody login(LoginVo vo);

    void logout(String accessToken);

    /**
     * 条件分页查询数据
     * @param vo
     * @param pageNo
     * @param pageSize
     * @return
     */
    Map<String, Object> page(QueryVo vo, int pageNo, int pageSize);

    void addUser(SysUser sysUser);

    void updateUser(SysUser user, String userId);

    /**
     * 根据id查询用户信息
     * @param id
     * @return
     */
    SysUser getUserInfoById(String id);

    /**
     * 查询用户拥有的角色数据接口
     * @param userId
     * @return
     */
    UserOwnRoleRespVo getUserOwnRole(String userId);

    /**
     * 保存用户所有的角色
     * @param vo
     */
    void setUserOwnRole(UserOwnRoleVo vo);

    /**
     * 修改用户信息
     * @param vo
     * @param userId
     */
    void updateUserInfo(UserUpdateVo vo, String userId);

    /**
     * 批量删除用户
     * @param list
     * @param userId
     */
    void deleteUsers(List<String> list, String userId);
}
