package com.pdx.service;

import com.pdx.entity.SysRole;

import java.util.List;

/**
 * @author PDX
 * @website https://blog.csdn.net/Gaowumao
 * @Date 2022-05-02 13:21
 * @Description
 */
public interface RoleService {
    /**
     * 根据用户Id获取角色信息
     * @param id
     * @return
     */
    List<String> getNamesByUserId(String id);

    /**
     * 获取所有角色列表
     * @return
     */
    List<SysRole> selectAll();

}
